package calin.adelin.lab3.ex3;

public class Autor
{

        private String name;
        private String email;
        private char gender;

        public Autor(String name, String email, char gender)
        {
            this.name = name;
            this.email = email;
            this.gender = gender;
        }

        public String getN()
        {
            return name;
        }

        public String getE()
        {
            return email;
        }

        public char getG()
        {
            return gender;
        }

        public void setE(String email)
        {
            this.email = email;
        }

        public void tooString()
        {
            System.out.println(this.name + " (" + this.gender + ") at " + this.email);
        }


}
