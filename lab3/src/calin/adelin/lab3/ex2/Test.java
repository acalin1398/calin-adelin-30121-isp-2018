package calin.adelin.lab3.ex2;


public class Test
{
    public static void main(String[] args)
    {

        Circle c1 = new Circle(10.0);
        Circle c2 = new Circle(14.0, "green");
        System.out.println(c1.getR());
        System.out.println(c1.getA());
        System.out.println(c2.getR());
        System.out.println(c2.getA());

    }
}
