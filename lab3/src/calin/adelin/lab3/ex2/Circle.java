package calin.adelin.lab3.ex2;

public class Circle
{
    private double radius;
    private String color;

    public Circle()
    {
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius)
    {
        this.radius = radius;
    }

    public Circle(double radius, String color)
    {
        this.radius = radius;
        this.color = color;
    }

    public double getR()
    {
        return radius;
    }

    public double getA()
    {
        return 2 * Math.PI * radius;
    }
}
