package calin.adelin.lab6.ex4;

public class Word {
    private String name;

    public Word(String name) {
        this.name = name;
    }



    public String toString() {
        return name;
    }
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Word))
            return false;
        Word x = (Word)obj;
        return name.equals(x.name);
    }

    public int hashCode() {
        return(name.length()*1000);
    }
}

