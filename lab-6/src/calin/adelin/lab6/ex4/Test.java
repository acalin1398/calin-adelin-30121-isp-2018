package calin.adelin.lab6.ex4;

import java.io.*;
public class Test {
    public static void main(String[] args) throws IOException {

        Dictionary dict = new Dictionary();
        char answer;
        String line;
        String definition;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Menu");
            System.out.println("a - Add word");
            System.out.println("s - Search word");
            System.out.println("p - Print words");
            System.out.println("d - Print definitions");
            System.out.println("k - Print dictionary");
            System.out.println("e - Exit");

            line = fluxIn.readLine();
            answer = line.charAt(0);

            switch (answer) {
                case 'a': case 'A':
                    System.out.println("Insert word:");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        System.out.println("Insert definition:");
                        definition = fluxIn.readLine();
                        dict.addWord(new Word(line), new Definition(definition));
                    }
                    break;
                case 's': case 'S':
                    System.out.println("Search word");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        Word x = new Word(line);
                        definition = String.valueOf(dict.getDefinition(x));

                        if (definition == null)
                            System.out.println("Word not found!");
                        else
                            System.out.println("Definition:" + definition);
                    }
                    break;
                case 'k': case 'K':
                    System.out.println("Print dictionary");
                    dict.printDictionary();
                    break;
                case 'p': case 'P':
                    System.out.println("Print words");
                    dict.getAllWords();
                    break;
                case 'd': case 'D':
                    System.out.println("Print definitions");
                    dict.getAllDefinitions();
                    break;

            }
        } while (answer != 'e' && answer != 'E');
        System.out.println("End of program!");



    }

}

