package calin.adelin.lab6.ex2;

public class Test {
    public static void main(String[] args) {
        Bank bk = new Bank();
        bk.addAccount("Micu",50);
        bk.addAccount("Nigel",4500);
        bk.addAccount("Bossman",95000);

        System.out.println("Bank Accounts(ordered by balance)");
        bk.printAccounts();
        System.out.println("Accounts that are in this interval (40;5000");
        bk.printAccounts(40,5000);
        System.out.println("Micu's account");
        bk.getAccount("Micu");
        System.out.println("Get all the accounts(ordered by name)");
        bk.getAllAccount();

    }
}
