package calin.adelin.lab7.ex4;

import java.util.Scanner;

public class CarParkMain {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        System.out.println("Introduceti numele grupului de masini:");
        String numeParcare = scan.nextLine();
        CarPark cp = new CarPark(numeParcare);

        Car c1 = new Car("Volkswagen", 5000);
        Car c2 = new Car("Opel", 2500);
        Car c3 = new Car("BMW", 1500);

        cp.add(c1);
        cp.add(c2);
        cp.add(c3);

        cp.viewCars();
        cp.save("C:\\Users\\AAA\\Documents\\calin-adelin-30121-isp-2018\\lab7\\src\\calin\\adelin\\lab7\\ex4\\Parkin");

        CarPark cp2 = CarPark.read("C:\\Users\\AAA\\Documents\\calin-adelin-30121-isp-2018\\lab7\\src\\calin\\adelin\\lab7\\ex4\\Parkin");
        cp2.viewCars();
        cp2.searchCar(c1);
    }
}
