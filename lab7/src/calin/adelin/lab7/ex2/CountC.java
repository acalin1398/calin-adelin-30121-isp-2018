package calin.adelin.lab7.ex2;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

    public class CountC {
        public static void main(String[] args) {
            Path file = Paths.get("C:\\Users\\AAA\\Documents\\calin-adelin-30121-isp-2018\\lab7\\src\\calin\\adelin\\lab7\\ex2\\data.txt");
            CountC cl1 = new CountC(file, 'a');
            System.out.println(cl1.count());
        }

        private Path file;
        private char lookFor;

        CountC(Path file, char lookFor){
            this.file = file;
            this.lookFor = lookFor;
        }

        private int count(){
            int count = 0;
            try(BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(file)))){
                String line = null;
                while((line = br.readLine()) != null){
                    for(int i = 0; i < line.length(); i++){
                        if(line.charAt(i) == lookFor){
                            count++;
                        }
                    }
                }
            } catch (IOException x){
                System.err.println(x);
            }
            return count;
        }
    }

