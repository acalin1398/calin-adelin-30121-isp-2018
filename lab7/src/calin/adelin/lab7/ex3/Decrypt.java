package calin.adelin.lab7.ex3;

import java.io.*;

public class Decrypt {
    public static void action(FileReader file, String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(file);
        String input = reader.readLine();
        System.out.println(input);

        char[] stringToCharArr = input.toCharArray();
        String outString = "";

        for(char crt:stringToCharArr){
            outString += String.valueOf(--crt);
        }

        System.out.println(outString);

        PrintWriter out = new PrintWriter("C:\\Users\\AAA\\Documents\\calin-adelin-30121-isp-2018\\lab7\\src\\calin\\adelin\\lab7\\ex3\\data.dec.txt");
        out.println(outString);
        out.close();
    }
}
