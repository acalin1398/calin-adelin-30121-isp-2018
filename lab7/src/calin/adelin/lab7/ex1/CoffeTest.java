package calin.adelin.lab7.ex1;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for(int i = 0;i<15;i++){
            Cofee c = mk.makeCofee();
            try {
                d.drinkCofee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            }
            finally{
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}//.class

class CofeeMaker {
    Cofee makeCofee(){
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        int n = (int)(Math.random()*100);
        Cofee cofee = new Cofee(t,c,n);
        return cofee;
    }

}//.class

class Cofee{
    private int temp;
    private int conc;
    public static int numb=0;

    Cofee(int t,int c, int n){temp = t;conc = c;numb= n;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getNumb(){return numb;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"]";}
}//.class

class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");
        System.out.println("Drink cofee:"+c);
        if(c.getNumb()>30)
            throw new ConcentrationException(c.getNumb(),"Cofee number too high!");
    }
}//.class

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }
}//.class

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
    class NumberException extends Exception
    {
        int n;
        public NumberException (int n, String msg)
        {
            super(msg);
            this.n=n;
        }
        int getNumb()
        {
            return n;
        }
    }
}//.class