package calin.adelin.lab4.ex3;

import calin.adelin.lab4.ex2.Author;

public class TestBook
{
    public static void main(String[] args)
    {
        Author a1= new Author("Adelin","adelin@yahoo.com", 'M');
        Book b1= new Book("Adelin","adelin@yahoo.com",'M',a1,"My Book",12.0);
        System.out.println("Author of book 1 "+ b1.getAuthor());
        System.out.println("Price of the book"+ b1.getPrice());
        b1.setPrice(11.99);
        System.out.println("New price of the book "+ b1.getPrice());
        System.out.println("Quantity of the book is "+ b1.getQtyInStock());
        b1.setQtyInStock(2);
        System.out.println("New quantity of the book is"+ b1.getQtyInStock());




    }
}
