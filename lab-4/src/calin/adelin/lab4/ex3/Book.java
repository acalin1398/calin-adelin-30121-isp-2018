package calin.adelin.lab4.ex3;


import calin.adelin.lab4.ex2.Author;

public class Book extends Author
{
    private String name;
    public Author author;
    private Double price;
    private int qtyInStock=0;



    public Book (String name, String email, char gender, Author author, String name1, Double price)
    {
        super(name,email, gender);
        this.name=name1;
        this.author=author;
        this.price=price;
    }

    public Book (String name, String email, char gender, Author author, String name1, Double price, int qtyInStock)
    {
        super(name,email, gender);

        this.name=name1;
        this.author=author;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }



    public String getName()
    {
        return name;
    }

    public String getAuthor()
    {

        return this.author.getName();
    }

    public Double getPrice()
    {
        return  this.price;
    }

    public void setPrice(double price)
    {
        this.price=price;
    }

    public int getQtyInStock()
    {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock)
    {
        this.qtyInStock=qtyInStock;
    }

    public String toString()
    {
        System.out.println("book-"+getName()+" by "+super.toString());
        return null;
    }

}
