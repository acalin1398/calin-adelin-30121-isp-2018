package calin.adelin.lab4.ex6;
import calin.adelin.lab4.ex6.Shape;
public class Rectangle extends Shape{
    double width=1.0;
    double length=1.0;;

    Rectangle(){
        width=1.0;
        length=1.0;
    }

    Rectangle(double width, double length){
        this.width=width;
        this.length=length;
    }

    Rectangle(double width, double length, String color, boolean filled){
        this.width=width;
        this.length=length;
        super.color=color;
        super.filled=filled;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth (double width){
        this.width=width;
    }

    public double getLength(){
        return length;
    }

    public void setLength(double length){
        this.length=length;
    }

    public double getArea(){
        return width*length;
    }

    public double getPerimetre(){
        return 2*width+2*length;
    }

    @Override public String toString(){
        return "A rectangle with width= "+width+" and length= "+length+", wich is a subclass of "+super.toString();
    }

    public static void main(String[] args){
        Rectangle r1=new Rectangle();
        System.out.println("Area= "+r1.getArea());
        System.out.println("Perimetre= "+r1.getPerimetre());
        System.out.println(r1.toString());
    }
}
