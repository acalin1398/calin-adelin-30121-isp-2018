package calin.adelin.lab4.ex6;
import calin.adelin.lab4.ex6.Shape;
public class Circle extends Shape{
    private double radius;
    private String color;

    Circle(){
        radius=1.0;
        color="red";
    }

    Circle (double radius){
        this.radius=radius;
    }

    Circle(double radius, String color){
        this.radius=radius;
        this.color=color;
    }

    public double getRadius(){
        return radius;
    }

    public String getColor(){
        return color;
    }

    public double getPerimeter(){
        return 2*3.14*radius;
    }

    @Override public String toString(){
        return "A circle with radius= " + radius +", which is a subclass of "+super.toString();
    }

    public static void main(String[] args){
        Circle c1 = new Circle();
        Circle c2 = new Circle(3.2);
        System.out.println(c1.getRadius());
        System.out.println(c1.getColor());
        System.out.println(c2.getRadius());
        System.out.println(c2.toString());

    }

}
