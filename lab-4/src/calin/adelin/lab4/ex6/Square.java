package calin.adelin.lab4.ex6;
import calin.adelin.lab4.ex6.Rectangle;
public class Square extends Rectangle{
    Square() {
        super.width=1.0;
        super.length=1.0;
    }

    Square(double size) {
        super.width=size;
        super.length=size;
    }

    Square(double size, String color, boolean filled){
        super.width=size;
        super.length=size;
        super.color=color;
        super.filled=filled;
    }

    public double getSide(){
        return width;
    }

    public void setSide (double side) {
        super.width = side;
        super.length = side;
    }

    public void setWidth(double side){
        super.width=side;
        super.length=side;
    }

    public void setLength (double side){
        super.length=side;
        super.width=side;
    }

    @Override public String toString(){
        return "A square with side= "+super.width+", wich is a subclass of "+super.toString();
    }

    public static void main(String[] args){
        Square s1=new Square();
        System.out.println(s1.getSide());
        System.out.println(s1.toString());
    }
}
