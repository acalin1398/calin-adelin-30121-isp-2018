package calin.adelin.lab4.ex4;

import calin.adelin.lab4.ex2.Author;



public class Bookk extends Author
{
    private String name;
    private double price;
    private Author[] author;
    private int qtyInStock = 0;

    public Bookk(String name, Author[] authors, double price){
        this.author=authors;
        this.name=name;
        this.price=price;
    }

    public Bookk(String name, Author[] authors, double price, int qtyInStock){
        this.author=authors;
        this.name=name;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    @Override
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Author[] getAuthors() {
        return author;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString(){
        String str = "Book called:"+this.name+" by "+this.getAuthors().length;
        return str;
    }

    public void printAuthors(){
        Author[] autori = getAuthors();
        for (int i = 0; i<autori.length;i++) System.out.println(autori[i].getName());
    }
}

