package calin.adelin.lab4.ex4;

import calin.adelin.lab4.ex2.Author;


public class TestBookk
{
    public static void main(String[] args) {
        Author a1 = new Author("autor1", "email@sait.ceva", 'm');
        Author a2 = new Author("autor2", "email@sait.ceva", 'm');
        Author a3 = new Author("autor3", "email@sait.ceva", 'f');
        Author a4 = new Author("autor4", "email@sait.ceva", 'f');

        Bookk b1 = new Bookk("carte1", new Author[]{a1,a2}, 10000, 10);
        Bookk b2 = new Bookk("carte2",new Author[]{a1,a2,a3,a4},100,1);

        b1.printAuthors();
        System.out.println(b2.toString());
        System.out.println(b2.getName());
    }
}

