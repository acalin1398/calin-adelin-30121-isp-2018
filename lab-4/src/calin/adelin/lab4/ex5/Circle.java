package calin.adelin.lab4.ex5;

public class Circle {
    private double radius;
    private String color;
    public Circle()
    {
        radius=1.0;
        color="red";
    }
    public Circle(double radius)
    {
        this.radius=radius;
    }
    public double getRadius()
    {
        return radius;
    }
    public double getArea()
    {
        return 3.14*radius*radius;
    }
    public static void main(String[] args)
    {
        Circle c1=new Circle();
        Circle c2=new Circle(2);
        double r=c2.getRadius();
        System.out.println("Radius c1:"+c1.getRadius());
        System.out.println("Radius c2:"+r);
        System.out.println("Area:"+c2.getArea());
    }
}
