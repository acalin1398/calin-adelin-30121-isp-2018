package calin.adelin.lab4.ex1;

public class Circle
{
    private double radius= 1.0;
    private String color="red";

    public Circle()
    {
       // this.radius = 1.0;
       // this.color = "red";
    }

   /* public Circle(double radius)
    {
        this.radius = radius;
    } */

    public Circle(double radius, String color)
    {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius()
    {
        return radius;
    }

    public double getArea()
    {
        return 2 * Math.PI * radius;
    }
}
