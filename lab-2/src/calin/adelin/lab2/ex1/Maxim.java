package calin.adelin.lab2.ex1;

import java.util.Scanner;

public class Maxim {

    public static void main(String args[]) throws InterruptedException
    {
        Scanner scnr = new Scanner(System.in);
        // Calculating Maximum two numbers in Java
                System.out.println("The two numbers are:");
        int a = scnr.nextInt();
        int b = scnr.nextInt();
        if (a == b)
            {
                System.out.printf("Numbers %d and %d, are equal", a, b);
                return;
            }

        else if (a > b)

            {
                System.out.printf("Between %d and %d, maximum is %d ", a, b, a);
            }
        else
            //a<b
            {
                System.out.printf("Between %d and %d, maximum number is %d ", a, b, b);
            }

        //or with Math.max!!!
        int max = Math.max(a, b);
                System.out.printf("Maximum value of %d and %d using Math.max() is %d %n", a, b, max);
    }



}