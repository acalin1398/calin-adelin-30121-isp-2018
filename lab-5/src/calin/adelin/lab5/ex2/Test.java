package calin.adelin.lab5.ex2;

public class Test {
    public static void main(String[] args){
        ProxyImage p = new ProxyImage("Real");
        p.display();
        p=new ProxyImage("Rotated");
        p.display();

    }
}