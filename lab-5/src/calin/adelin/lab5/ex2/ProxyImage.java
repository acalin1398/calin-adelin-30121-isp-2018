package calin.adelin.lab5.ex2;

public class ProxyImage implements Image{

    private Image image;
    private String fileName;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public void display() {
        if(getFileName() == "Real"){
            image = new RealImage(fileName);
        }
        else if(getFileName()=="Rotated"){
            image=new RotatedImage(fileName);
        }
        image.display();
    }
}