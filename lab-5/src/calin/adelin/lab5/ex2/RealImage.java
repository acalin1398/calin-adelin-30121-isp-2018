package calin.adelin.lab5.ex2;

public class RealImage implements Image {

    private String fileName;


    public RealImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void display() {
        System.out.println("Real Image displayed " );}

}