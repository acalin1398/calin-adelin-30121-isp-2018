package calin.adelin.lab10.ex6;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;


public class StopWatch extends JFrame implements ActionListener,Runnable
{
    JLabel disp;
    JButton btn1,btn2;
    boolean stop=false;
    boolean reset=false;
    int i,j,k,l;
    public StopWatch()
    {
        disp=new JLabel();
        btn1=new JButton("Start");
        btn2=new JButton("Reset");
        disp.setFont(new Font("Helvetica",Font.PLAIN,20));
        disp.setBackground(Color.cyan);
        disp.setForeground(Color.red);
        Container c=getContentPane();
        c.setLayout(new FlowLayout());
        c.add(disp); c.add(btn1); c.add(btn2);
        btn1.addActionListener(this);
        btn2.addActionListener(this);
    }
    public void run()
    {
        for(i=0;;i++)
        {
            for(j=0;j< 60;j++)
            {
                for(k=0;k< 60;k++)
                {
                    for(l=0;l< 100;l++)
                    {
                        if(stop)
                        {
                            break;
                        }
                        if(reset)
                        {
                            i=0;
                            j=0;
                            k=0;
                            l=0;
                        }
                        NumberFormat nf = new DecimalFormat("00");
                        disp.setText(nf.format(i)+":"+nf.format(j)+":"+nf.format(k)+":"+nf.format(l));
                        try{
                            Thread.sleep(10);
                        }catch(Exception e){}
                    }
                }
            }
        }
    }
    public void actionPerformed(ActionEvent ae)
    {
        Thread t=new Thread(this);
        if(ae.getActionCommand().equals("Start"))
        {
            t.start();
            btn1.setText("Stop");
        }
        else
        {
            stop=true;
        }

        Thread t1=new Thread(this);
        if(ae.getActionCommand().equals("Reset"))
        {
            reset=true;
            t1.start();
        }
    }
    public static void main(String[] args)
    {
        StopWatch s=new StopWatch();
        s.setSize(500,100);
        s.setVisible(true);
        s.setTitle("StopWatch");
        s.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}